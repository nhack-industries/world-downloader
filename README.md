This is a simple world downloader mod for forge. It will automatically save chunks in `config/wdl-saves/`, under the name of the server you were playing on.

# Building

- Make sure you have a gradle installation, or bootstrap gradle wrapper by running `gradlew`.
- Run `gradle spotlessapply` and `gradle build`.

# Installation

Copy the jar from `build\libs` into your forge mods folder.