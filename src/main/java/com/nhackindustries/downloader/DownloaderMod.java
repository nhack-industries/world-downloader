package com.nhackindustries.downloader;

import com.nhackindustries.downloader.tracking.EntityTracker;
import it.unimi.dsi.fastutil.longs.Long2ObjectMap;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.SocketAddress;
import java.nio.file.Files;
import java.nio.file.Path;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.ChunkProviderClient;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.datafix.DataFixesManager;
import net.minecraft.world.MinecraftException;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraft.world.chunk.storage.AnvilSaveHandler;
import net.minecraft.world.chunk.storage.IChunkLoader;
import net.minecraft.world.storage.SaveHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.world.ChunkDataEvent;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.FMLNetworkEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = DownloaderMod.MODID, name = DownloaderMod.NAME, version = DownloaderMod.VERSION)
public class DownloaderMod {
  public static final String MODID = "world-downloader";
  public static final String NAME = "world-downloader";
  public static final String VERSION = "1.0.0";

  private static Logger logger = null;
  private static Path configDirectory = null;
  private static Path saveDirectory = null;
  private String hostname = null;
  private String port = null;
  private SaveHandler saveHandler = null;
  private IChunkLoader chunkLoader = null;

  private final EntityTracker entityTracker;
  private final DownloaderWorldListener worldListener;

  public DownloaderMod() {
    entityTracker = new EntityTracker();
    worldListener = new DownloaderWorldListener(entityTracker);
  }

  @Mod.EventHandler
  public void preInit(FMLPreInitializationEvent event) {
    if (event.getSide().isServer()) {
      return;
    }

    logger = event.getModLog();
    configDirectory = event.getModConfigurationDirectory().toPath();
    saveDirectory = configDirectory.resolve("wdl-saves/");
    try {
      if (!Files.exists(saveDirectory)) Files.createDirectories(saveDirectory);
    } catch (IOException exception) {
      logger.error("Couldn't create saves folder");
    }
  }

  @Mod.EventHandler
  public void init(FMLInitializationEvent event) {
    if (event.getSide().isServer()) {
      return;
    }

    if (saveDirectory == null) return;

    MinecraftForge.EVENT_BUS.register(this);
  }

  @SubscribeEvent
  public void onServerJoined(FMLNetworkEvent.ClientConnectedToServerEvent event) {
    if (saveHandler != null && chunkLoader != null) {
      chunkLoader.flush();
      chunkLoader = null;
      saveHandler = null;
    }

    SocketAddress address = event.getManager().getRemoteAddress();
    if (address instanceof InetSocketAddress) {
      InetSocketAddress inetAddress = (InetSocketAddress) address;
      hostname = inetAddress.getHostName();
      port = String.valueOf(inetAddress.getPort());
      saveHandler =
          new AnvilSaveHandler(
              saveDirectory.toFile(), hostname + "_" + port, false, DataFixesManager.createFixer());
    }
  }

  @SubscribeEvent
  public void onWorldLoaded(WorldEvent.Load event) {
    World world = event.getWorld();
    if (!(world instanceof WorldClient)) return;
    WorldClient worldClient = (WorldClient) world;
    if (!world.isRemote) return;

    if (saveHandler == null) return;

    worldClient.addEventListener(worldListener);
    chunkLoader = saveHandler.getChunkLoader(worldClient.provider);
  }

  @SubscribeEvent
  public void onWorldUnloaded(WorldEvent.Unload event) {
    World world = event.getWorld();
    if (!(world instanceof WorldClient)) return;
    WorldClient worldClient = (WorldClient) world;
    if (!world.isRemote) return;

    if (saveHandler == null) return;

    ChunkProviderClient chunkProvider = worldClient.getChunkProvider();
    Long2ObjectMap<Chunk> loadedChunks =
        ObfuscationReflectionHelper.getPrivateValue(
            ChunkProviderClient.class, chunkProvider, "field_73236_b");

    loadedChunks.values().forEach(Chunk::onUnload);
    saveHandler.flush();
  }

  @SubscribeEvent
  public void onSaveChunk(ChunkDataEvent.Save event) {
    Chunk chunk = event.getChunk();
    NBTTagCompound chunkData = event.getData();
    NBTTagCompound levelData = chunkData.getCompoundTag("Level");
    NBTTagList entityList = levelData.getTagList("Entities", 10);
    NBTTagList tileEntityList = levelData.getTagList("TileEntities", 10);

    entityTracker.saveTrackedEntities(chunk, entityList);
    entityTracker.saveTrackedTileEntities(chunk, tileEntityList);
  }

  @SubscribeEvent
  public void onChunkUnloaded(ChunkEvent.Unload event) {
    if (saveHandler == null) return;

    WorldClient world = Minecraft.getMinecraft().world;
    if (world == null || !world.isRemote) return;

    Chunk chunk = event.getChunk();
    logger.debug("Unloading chunk {} {} - empty = {}", chunk.x, chunk.z, chunk.isEmpty());
    if (chunk.isEmpty()) return;

    try {
      chunkLoader.saveChunk(world, event.getChunk());
    } catch (MinecraftException | IOException e) {
      e.printStackTrace();
    }
  }
}
