package com.nhackindustries.downloader.tracking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.util.ClassInheritanceMultiMap;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.chunk.Chunk;

public class EntityTracker {
  private static final List<Class<? extends Entity>> BLACKLISTED_ENTITY_TYPES = new ArrayList<>();
  private final Map<UUID, TrackedEntity> entities = new HashMap<>();

  public void trackEntity(Entity entity) {
    if (isBlacklisted(entity)) {
      return;
    } else if (entity.isDead) {
      entities.remove(entity.getPersistentID());
      return;
    }

    entities.put(entity.getPersistentID(), new TrackedEntity(entity));
  }

  public void saveTrackedEntities(Chunk chunk, NBTTagList savedEntityList) {
    List<UUID> savedEntities = new ArrayList<>();
    for (ClassInheritanceMultiMap<Entity> entityList : chunk.getEntityLists()) {
      for (Entity entity : entityList) {
        savedEntities.add(entity.getPersistentID());
      }
    }

    savedEntities.forEach(entities::remove);

    List<TrackedEntity> entitiesToSave = new ArrayList<>();
    for (TrackedEntity entity : entities.values()) {
      Vec3d position = entity.getPosition();
      int chunkX = MathHelper.floor(position.x / 16.0);
      int chunkZ = MathHelper.floor(position.z / 16.0);
      if (chunkX == chunk.x && chunkZ == chunk.z) {
        entitiesToSave.add(entity);
      }
    }

    for (TrackedEntity entity : entitiesToSave) {
      NBTTagCompound serializedEntity = entity.getSerializedEntity();
      if (serializedEntity.isEmpty()) {
        continue;
      }

      savedEntityList.appendTag(serializedEntity);
    }

    entitiesToSave.stream().map(TrackedEntity::getEntityId).forEach(entities::remove);
  }

  public void saveTrackedTileEntities(Chunk chunk, NBTTagList savedTileEntityList) {}

  private boolean isBlacklisted(Entity entity) {
    for (Class<? extends Entity> type : BLACKLISTED_ENTITY_TYPES) {
      if (entity.getClass().isAssignableFrom(type)) return true;
    }

    return false;
  }

  static {
    BLACKLISTED_ENTITY_TYPES.add(EntityPlayer.class);
  }
}
