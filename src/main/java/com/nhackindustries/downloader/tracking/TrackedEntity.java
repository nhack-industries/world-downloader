package com.nhackindustries.downloader.tracking;

import java.util.UUID;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.math.Vec3d;

public class TrackedEntity {
  private final UUID entityId;
  private final Vec3d position;
  private final NBTTagCompound serializedEntity = new NBTTagCompound();

  public TrackedEntity(Entity entity) {
    entityId = entity.getPersistentID();
    position = entity.getPositionVector();
    entity.writeToNBTAtomically(serializedEntity);
  }

  public UUID getEntityId() {
    return entityId;
  }

  public Vec3d getPosition() {
    return position;
  }

  public NBTTagCompound getSerializedEntity() {
    return serializedEntity;
  }
}
